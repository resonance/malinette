import oscP5.*;
import netP5.*;
import processing.serial.*;
import pt.citar.diablu.processing.mindset.*;

boolean SEND_OSC = true;
String OSC_ADDRESS = "127.0.0.1";
int OSC_PORT = 54321;
String SERIAL_PORT = "/dev/tty.MindWaveMobile-DevA";

OscP5 oscP5;
NetAddress myRemoteLocation; 
MindSet mindSet;

void setup() {
  frameRate(20);
  oscP5 = new OscP5(this, 12345);
  myRemoteLocation = new NetAddress(OSC_ADDRESS, OSC_PORT);
  mindSet = new MindSet(this, SERIAL_PORT);
}

void draw() {  
}

void exit() {
  //println("Exiting");
  mindSet.quit();
  super.exit();
}

public void attentionEvent(int attentionLevel) {
    OscMessage myMessage = new OscMessage("/mindset/attentionlevel");
    myMessage.add(attentionLevel);
    oscP5.send(myMessage, myRemoteLocation);
}

public void meditationEvent(int meditationLevel) {
    OscMessage myMessage = new OscMessage("/mindset/meditationlevel");
    myMessage.add(meditationLevel);
    oscP5.send(myMessage, myRemoteLocation);
}

public void eegEvent(int delta, int theta, int low_alpha, int high_alpha, int low_beta, int high_beta, int low_gamma, int mid_gamma) {
    OscMessage myMessage = new OscMessage("/mindset/eeg");
    myMessage.add(delta);
    myMessage.add(theta);
    myMessage.add(low_alpha);
    myMessage.add(high_alpha);
    myMessage.add(low_beta);
    myMessage.add(high_beta);
    myMessage.add(low_gamma);
    myMessage.add(mid_gamma);
    oscP5.send(myMessage, myRemoteLocation);
} 
