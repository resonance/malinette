#! /bin/sh
# Rename abstractions into files
# Usage : bash rename_abstraction.sh folder/abstraction_old folder/abstraction_new
# Example : bash rename_abstraction.sh video/video-in video/video-camera

# Get filenames
file_old=$(basename "$1")
file_new=$(basename "$2")

# Change the name of the file
mv "../../abstractions/$1.pd" "../../abstractions/$2.pd"
mv "../../abstractions/$1-help.pd" "../../abstractions/$2-help.pd"

# Change the name into all other files (patchs) : abstractions, examples, projects
pattern="../../*/*/*.pd"
for file in $(ls -R -d $pattern) ; do
    sed -i -e "s/^\(#X obj [0-9]\+ [0-9]\+\) $file_old\([ ;]\)/\1 $file_new\2/" $file
done


# Todo : change the title in the help patch + change the name in the default list
