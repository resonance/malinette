
################### ARCHIVES ######################
#filename1="${$1##*/}"
#dirname1="${$1%/*}"
#dir_old=$(dirname "$1")
#dir_new=$(dirname "$2")

sed -i "s/$file1/$file2/g" "../../abstractions/$2-help.pd"
DIR1=$(echo $dir1 | tr [a-z] [A-Z])
sed -i "s/$file1/$file2/g" "../../abstractions/$dir1/ALL-$DIR1-help.pd"
cd ../../examples/
find . -name "*.pd" -type f -exec sed -i "s/$file1\([; ]\)/$file2\1/g" {} \;
cd ../projects/
find . -name "*.pd" -type f -exec sed -i "s/$file1\([; ]\)/$file2\1/g" {} \;

echo $file1
echo $file2
echo $dir1
echo $dir2

# Example with abstractions/video/video-in.pd > video-camera
sed -i -e 's/^\(#X obj [0-9]\+ [0-9]\+\) video-in\([ ;]\)/\1 video-camera\2/' *.pd
sed -i -e 's/^\(#X obj [0-9]\+ [0-9]\+\) video-in\([ ;]\)/\1 video-camera\2/' abstractions/video/*.pd
sed -i -e 's/^\(#X obj [0-9]\+ [0-9]\+\) video-in\([ ;]\)/\1 video-camera\2/' examples/examples-fr/*.pd
sed -i -e 's/^\(#X obj [0-9]\+ [0-9]\+\) video-in\([ ;]\)/\1 video-camera\2/' examples/examples-en/*.pd
sed -i -e 's/^\(#X obj [0-9]\+ [0-9]\+\) video-in\([ ;]\)/\1 video-camera\2/' examples/manual-fr/*.pd
sed -i -e 's/^\(#X obj [0-9]\+ [0-9]\+\) video-in\([ ;]\)/\1 video-camera\2/' examples/manual-en/*.pd
